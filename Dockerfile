FROM openjdk:8u141-jdk

RUN mkdir /app

WORKDIR /app

COPY target/pelite-integrator-1.0.0.jar /app/

# Can be one of {local,dev,qa,uat,prod}
ENV ENVIRONMENT dev

EXPOSE 8080 8000

ENTRYPOINT exec java -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n -Djava.security.egd=file:/dev/./urandom -Dspring.profiles.active=$ENVIRONMENT -jar /app/pelite-integrator-1.0.0.jar


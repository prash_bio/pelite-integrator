package com.arrow.pricing.service;

import com.arrow.pricing.model.PelitePriceRequestItem;

import java.util.Date;

public class PelitePriceRequestItemBuilder {

    private Long sourceLineReference;

    private Date pricingDate;

    private Long itemId;

    private String arrowPartnumber;

    private String customerPartNumber;

    private Long warehouseId;

    private Long quantity;

    public PelitePriceRequestItem build() {
        return new PelitePriceRequestItem(sourceLineReference, pricingDate, itemId, arrowPartnumber, customerPartNumber, warehouseId, quantity);
    }

    public PelitePriceRequestItemBuilder withSourceLineReference(Long sourceLineReference) {
        this.sourceLineReference = sourceLineReference;
        return this;
    }

    public PelitePriceRequestItemBuilder withPricingDate(Date pricingDate) {
        this.pricingDate = pricingDate;
        return this;
    }

    public PelitePriceRequestItemBuilder withItemId(Long itemId) {
        this.itemId = itemId;
        return this;
    }

    public PelitePriceRequestItemBuilder withArrowPartnumber(String arrowPartnumber) {
        this.arrowPartnumber = arrowPartnumber;
        return this;
    }

    public PelitePriceRequestItemBuilder withCustomerPartNumber(String customerPartNumber) {
        this.customerPartNumber = customerPartNumber;
        return this;
    }

    public PelitePriceRequestItemBuilder withWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
        return this;
    }

    public PelitePriceRequestItemBuilder withQuantity(Long quantity) {
        this.quantity = quantity;
        return this;
    }
}
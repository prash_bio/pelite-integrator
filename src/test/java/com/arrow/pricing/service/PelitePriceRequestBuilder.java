package com.arrow.pricing.service;

import com.arrow.pricing.model.PelitePriceRequest;
import com.arrow.pricing.model.PelitePriceRequestItem;

import java.util.List;

public class PelitePriceRequestBuilder {

    private Long requestReference;

    private Long appUserId;

    private Long appRespId;

    private Long appRespApplId;

    private String requestType;

    private Long billToSiteUseId;

    private Long shipToSiteUseId;

    private Long endCustSiteUseId;

    private String currencyCode;

    private List<PelitePriceRequestItem> items;

    private String region;

    public PelitePriceRequest build() {
        return new PelitePriceRequest(requestReference, appUserId, appRespId, appRespApplId, requestType, billToSiteUseId, shipToSiteUseId, endCustSiteUseId, currencyCode, items, region);
    }

    public PelitePriceRequestBuilder withRequestReference(Long requestReference) {
        this.requestReference = requestReference;
        return this;
    }

    public PelitePriceRequestBuilder withAppUserId(Long appUserId) {
        this.appUserId = appUserId;
        return this;
    }

    public PelitePriceRequestBuilder withAppRespId(Long appRespId) {
        this.appRespId = appRespId;
        return this;
    }

    public PelitePriceRequestBuilder withAppRespApplId(Long appRespApplId) {
        this.appRespApplId = appRespApplId;
        return this;
    }

    public PelitePriceRequestBuilder withRequestType(String requestType) {
        this.requestType = requestType;
        return this;
    }

    public PelitePriceRequestBuilder withBillToSiteUseId(Long billToSiteUseId) {
        this.billToSiteUseId = billToSiteUseId;
        return this;
    }

    public PelitePriceRequestBuilder withShipToSiteUseId(Long shipToSiteUseId) {
        this.shipToSiteUseId = shipToSiteUseId;
        return this;
    }

    public PelitePriceRequestBuilder withEndCustSiteUseId(Long endCustSiteUseId) {
        this.endCustSiteUseId = endCustSiteUseId;
        return this;
    }

    public PelitePriceRequestBuilder withCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
        return this;
    }

    public PelitePriceRequestBuilder withItems(List<PelitePriceRequestItem> items) {
        this.items = items;
        return this;
    }

    public PelitePriceRequestBuilder withRegion(String region) {
        this.region = region;
        return this;
    }

}
package com.arrow.pricing.service;

import com.arrow.pricing.model.PelitePriceRequest;
import com.arrow.pricing.model.PelitePriceRequestItem;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class JSONMarshallerTest {

    @Test
    public void testMarshalling() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);

        PelitePriceRequestItem pelitePriceRequestItem = new PelitePriceRequestItemBuilder().withSourceLineReference(new Long(1)).withItemId(new Long(6225986)).withCustomerPartNumber("").withWarehouseId(new Long(1596)).withQuantity(new Long(90)).build();
        List<PelitePriceRequestItem> items = new ArrayList<PelitePriceRequestItem>();
        items.add(pelitePriceRequestItem);
        PelitePriceRequest pelitePriceRequest = new PelitePriceRequestBuilder().withAppUserId(new Long(7137)).withAppRespId(new Long(51158)).withAppRespApplId(new Long(660)).withRequestType("ALL").withBillToSiteUseId(new Long(2193799)).withShipToSiteUseId(new Long(2233347)).withCurrencyCode("USD").withItems(items).withRegion("AC").build();
        pelitePriceRequest.setAppRespId(new Long(51158));

        String s = null;
        try {
            s = mapper.writeValueAsString(pelitePriceRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        System.out.println(s);
    }
}
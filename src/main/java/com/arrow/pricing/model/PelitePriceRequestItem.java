package com.arrow.pricing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PelitePriceRequestItem implements Serializable {

    //To allow spring to instantiate this class.
    public PelitePriceRequestItem() {

    }

    private Long sourceLineReference;

    private Date pricingDate;

    private Long itemId;

    private String arrowPartnumber;

    private String customerPartNumber;

    private Long warehouseId;

    private Long quantity;

    public PelitePriceRequestItem(Long sourceLineReference, Date pricingDate, Long itemId, String arrowPartnumber, String customerPartNumber, Long warehouseId, Long quantity) {
        this.sourceLineReference = sourceLineReference;
        this.pricingDate = pricingDate;
        this.itemId = itemId;
        this.arrowPartnumber = arrowPartnumber;
        this.customerPartNumber = customerPartNumber;
        this.warehouseId = warehouseId;
        this.quantity = quantity;
    }

    @JsonProperty("sourceLineReference")
    public Long getSourceLineReference() {
        return sourceLineReference;
    }

    public void setSourceLineReference(Long sourceLineReference) {
        this.sourceLineReference = sourceLineReference;
    }

    @JsonProperty("pricingDate")
    public Date getPricingDate() {
        return pricingDate;
    }

    public void setPricingDate(Date pricingDate) {
        this.pricingDate = pricingDate;
    }

    @JsonProperty("itemId")
    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("arrowPartnumber")
    public String getArrowPartnumber() {
        return arrowPartnumber;
    }

    public void setArrowPartnumber(String arrowPartnumber) {
        this.arrowPartnumber = arrowPartnumber;
    }

    @JsonProperty("customerPartNumber")
    public String getCustomerPartNumber() {
        return customerPartNumber;
    }

    public void setCustomerPartNumber(String customerPartNumber) {
        this.customerPartNumber = customerPartNumber;
    }

    @JsonProperty("warehouseId")
    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    @JsonProperty("quantity")
    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }
}
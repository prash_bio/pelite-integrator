package com.arrow.pricing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PelitePricingResponseBreaks implements Serializable {

    public PelitePricingResponseBreaks() {

    }

    private BigDecimal quantityFrom;

    private BigDecimal quantityTo;

    private BigDecimal resalePrice;

    private BigDecimal businessCost;

    @JsonProperty("quantityFrom")
    public BigDecimal getQuantityFrom() {
        return quantityFrom;
    }

    public void setQuantityFrom(BigDecimal quantityFrom) {
        this.quantityFrom = quantityFrom;
    }

    @JsonProperty("quantityTo")
    public BigDecimal getQuantityTo() {
        return quantityTo;
    }

    public void setQuantityTo(BigDecimal quantityTo) {
        this.quantityTo = quantityTo;
    }

    @JsonProperty("resalePrice")
    public BigDecimal getResalePrice() {
        return resalePrice;
    }

    public void setResalePrice(BigDecimal resalePrice) {
        this.resalePrice = resalePrice;
    }

    @JsonProperty("businessCost")
    public BigDecimal getBusinessCost() {
        return businessCost;
    }

    public void setBusinessCost(BigDecimal businessCost) {
        this.businessCost = businessCost;
    }
}
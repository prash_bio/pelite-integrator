package com.arrow.pricing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PelitePricingResponseAddntlDetails implements Serializable {

    //This is to allow marshaller to instantiate this class.
    public PelitePricingResponseAddntlDetails() {

    }

    private BigDecimal endCustRecordId;

    private Long endCustomerId;

    private BigDecimal endCustomerNumber;

    private String endCustomerName;

    private BigDecimal endCustSiteUseId;

    private String endCustSiteName;

    private String customerPartnumber;

    @JsonProperty("endCustRecordId")
    public BigDecimal getEndCustRecordId() {
        return endCustRecordId;
    }

    public void setEndCustRecordId(BigDecimal endCustRecordId) {
        this.endCustRecordId = endCustRecordId;
    }

    @JsonProperty("endCustomerId")
    public Long getEndCustomerId() {
        return endCustomerId;
    }

    public void setEndCustomerId(Long endCustomerId) {
        this.endCustomerId = endCustomerId;
    }

    @JsonProperty("endCustomerNumber")
    public BigDecimal getEndCustomerNumber() {
        return endCustomerNumber;
    }

    public void setEndCustomerNumber(BigDecimal endCustomerNumber) {
        this.endCustomerNumber = endCustomerNumber;
    }

    @JsonProperty("endCustomerName")
    public String getEndCustomerName() {
        return endCustomerName;
    }

    public void setEndCustomerName(String endCustomerName) {
        this.endCustomerName = endCustomerName;
    }

    @JsonProperty("endCustSiteUseId")
    public BigDecimal getEndCustSiteUseId() {
        return endCustSiteUseId;
    }

    public void setEndCustSiteUseId(BigDecimal endCustSiteUseId) {
        this.endCustSiteUseId = endCustSiteUseId;
    }

    @JsonProperty("endCustSiteName")
    public String getEndCustSiteName() {
        return endCustSiteName;
    }

    public void setEndCustSiteName(String endCustSiteName) {
        this.endCustSiteName = endCustSiteName;
    }

    @JsonProperty("customerPartnumber")
    public String getCustomerPartnumber() {
        return customerPartnumber;
    }

    public void setCustomerPartnumber(String customerPartnumber) {
        this.customerPartnumber = customerPartnumber;
    }
}
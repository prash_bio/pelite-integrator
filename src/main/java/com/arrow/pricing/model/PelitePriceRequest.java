package com.arrow.pricing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PelitePriceRequest implements Serializable {

    //To allow spring to instantiate this class.
    public PelitePriceRequest() {

    }

    private Long requestReference;

    private Long userId;

    private String password;

    private String sourceSystem;

    private String targetSystem;

    private Long appUserId;

    private Long appRespId;

    private Long appRespApplId;

    private String requestType;

    private Long billToSiteUseId;

    private Long shipToSiteUseId;

    private Long endCustSiteUseId;

    private String currencyCode;

    private List<PelitePriceRequestItem> items;

    private String region;

    public PelitePriceRequest(Long requestReference, Long appUserId, Long appRespId, Long appRespApplId, String requestType, Long billToSiteUseId, Long shipToSiteUseId, Long endCustSiteUseId, String currencyCode, List<PelitePriceRequestItem> items, String region) {
        this.requestReference = requestReference;
        this.appUserId = appUserId;
        this.appRespApplId = appRespApplId;
        this.requestType = requestType;
        this.billToSiteUseId = billToSiteUseId;
        this.shipToSiteUseId = shipToSiteUseId;
        this.endCustSiteUseId = endCustSiteUseId;
        this.currencyCode = currencyCode;
        this.items = items;
        this.region = region;
    }

    @JsonProperty("userId")
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("sourceSystem")
    public String getSourceSystem() {
        return sourceSystem;
    }

    public void setSourceSystem(String sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    @JsonProperty("targetSystem")
    public String getTargetSystem() {
        return targetSystem;
    }

    public void setTargetSystem(String targetSystem) {
        this.targetSystem = targetSystem;
    }

    @JsonProperty("requestReference")
    public Long getRequestReference() {
        return requestReference;
    }

    public void setRequestReference(Long requestReference) {
        this.requestReference = requestReference;
    }

    @JsonProperty("appUserId")
    public Long getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(Long appUserId) {
        this.appUserId = appUserId;
    }

    @JsonProperty("appRespId")
    public Long getAppRespId() {
        return appRespId;
    }

    public void setAppRespId(Long appRespId) {
        this.appRespId = appRespId;
    }

    @JsonProperty("appRespApplId")
    public Long getAppRespApplId() {
        return appRespApplId;
    }

    public void setAppRespApplId(Long appRespApplId) {
        this.appRespApplId = appRespApplId;
    }

    @JsonProperty("requestType")
    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @JsonProperty("billToSiteUseId")
    public Long getBillToSiteUseId() {
        return billToSiteUseId;
    }

    public void setBillToSiteUseId(Long billToSiteUseId) {
        this.billToSiteUseId = billToSiteUseId;
    }

    @JsonProperty("shipToSiteUseId")
    public Long getShipToSiteUseId() {
        return shipToSiteUseId;
    }

    public void setShipToSiteUseId(Long shipToSiteUseId) {
        this.shipToSiteUseId = shipToSiteUseId;
    }

    @JsonProperty("endCustSiteUseId")
    public Long getEndCustSiteUseId() {
        return endCustSiteUseId;
    }

    public void setEndCustSiteUseId(Long endCustSiteUseId) {
        this.endCustSiteUseId = endCustSiteUseId;
    }

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @JsonProperty("items")
    public List<PelitePriceRequestItem> getItems() {
        return items;
    }

    public void setItems(List<PelitePriceRequestItem> items) {
        this.items = items;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
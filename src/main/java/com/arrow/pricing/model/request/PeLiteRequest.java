
package com.arrow.pricing.model.request;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "userId",
    "password",
    "sourceSystem",
    "targetSystem",
    "requestReference",
    "appUserId",
    "appRespId",
    "appRespApplId",
    "requestType",
    "billToSiteUseId",
    "shipToSiteUseId",
    "endCustSiteUseId",
    "currencyCode",
    "items",
    "region"
})
public class PeLiteRequest {

    @JsonProperty("userId")
    private Object userId;
    @JsonProperty("password")
    private Object password;
    @JsonProperty("sourceSystem")
    private Object sourceSystem;
    @JsonProperty("targetSystem")
    private Object targetSystem;
    @JsonProperty("requestReference")
    private Integer requestReference;
    @JsonProperty("appUserId")
    private Integer appUserId;
    @JsonProperty("appRespId")
    private Integer appRespId;
    @JsonProperty("appRespApplId")
    private Integer appRespApplId;
    @JsonProperty("requestType")
    private String requestType;
    @JsonProperty("billToSiteUseId")
    private Integer billToSiteUseId;
    @JsonProperty("shipToSiteUseId")
    private Integer shipToSiteUseId;
    @JsonProperty("endCustSiteUseId")
    private Integer endCustSiteUseId;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("items")
    private List<Item> items = new ArrayList<Item>();
    @JsonProperty("region")
    private String region;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("userId")
    public Object getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(Object userId) {
        this.userId = userId;
    }

    @JsonProperty("password")
    public Object getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(Object password) {
        this.password = password;
    }

    @JsonProperty("sourceSystem")
    public Object getSourceSystem() {
        return sourceSystem;
    }

    @JsonProperty("sourceSystem")
    public void setSourceSystem(Object sourceSystem) {
        this.sourceSystem = sourceSystem;
    }

    @JsonProperty("targetSystem")
    public Object getTargetSystem() {
        return targetSystem;
    }

    @JsonProperty("targetSystem")
    public void setTargetSystem(Object targetSystem) {
        this.targetSystem = targetSystem;
    }

    @JsonProperty("requestReference")
    public Integer getRequestReference() {
        return requestReference;
    }

    @JsonProperty("requestReference")
    public void setRequestReference(Integer requestReference) {
        this.requestReference = requestReference;
    }

    @JsonProperty("appUserId")
    public Integer getAppUserId() {
        return appUserId;
    }

    @JsonProperty("appUserId")
    public void setAppUserId(Integer appUserId) {
        this.appUserId = appUserId;
    }

    @JsonProperty("appRespId")
    public Integer getAppRespId() {
        return appRespId;
    }

    @JsonProperty("appRespId")
    public void setAppRespId(Integer appRespId) {
        this.appRespId = appRespId;
    }

    @JsonProperty("appRespApplId")
    public Integer getAppRespApplId() {
        return appRespApplId;
    }

    @JsonProperty("appRespApplId")
    public void setAppRespApplId(Integer appRespApplId) {
        this.appRespApplId = appRespApplId;
    }

    @JsonProperty("requestType")
    public String getRequestType() {
        return requestType;
    }

    @JsonProperty("requestType")
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    @JsonProperty("billToSiteUseId")
    public Integer getBillToSiteUseId() {
        return billToSiteUseId;
    }

    @JsonProperty("billToSiteUseId")
    public void setBillToSiteUseId(Integer billToSiteUseId) {
        this.billToSiteUseId = billToSiteUseId;
    }

    @JsonProperty("shipToSiteUseId")
    public Integer getShipToSiteUseId() {
        return shipToSiteUseId;
    }

    @JsonProperty("shipToSiteUseId")
    public void setShipToSiteUseId(Integer shipToSiteUseId) {
        this.shipToSiteUseId = shipToSiteUseId;
    }

    @JsonProperty("endCustSiteUseId")
    public Integer getEndCustSiteUseId() {
        return endCustSiteUseId;
    }

    @JsonProperty("endCustSiteUseId")
    public void setEndCustSiteUseId(Integer endCustSiteUseId) {
        this.endCustSiteUseId = endCustSiteUseId;
    }

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    @JsonProperty("currencyCode")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @JsonProperty("items")
    public List<Item> getItems() {
        return items;
    }

    @JsonProperty("items")
    public void setItems(List<Item> items) {
        this.items = items;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("userId", userId).append("password", password).append("sourceSystem", sourceSystem).append("targetSystem", targetSystem).append("requestReference", requestReference).append("appUserId", appUserId).append("appRespId", appRespId).append("appRespApplId", appRespApplId).append("requestType", requestType).append("billToSiteUseId", billToSiteUseId).append("shipToSiteUseId", shipToSiteUseId).append("endCustSiteUseId", endCustSiteUseId).append("currencyCode", currencyCode).append("items", items).append("region", region).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(requestType).append(sourceSystem).append(requestReference).append(appRespApplId).append(userId).append(targetSystem).append(password).append(appRespId).append(billToSiteUseId).append(shipToSiteUseId).append(additionalProperties).append(appUserId).append(endCustSiteUseId).append(region).append(currencyCode).append(items).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof PeLiteRequest) == false) {
            return false;
        }
        PeLiteRequest rhs = ((PeLiteRequest) other);
        return new EqualsBuilder().append(requestType, rhs.requestType).append(sourceSystem, rhs.sourceSystem).append(requestReference, rhs.requestReference).append(appRespApplId, rhs.appRespApplId).append(userId, rhs.userId).append(targetSystem, rhs.targetSystem).append(password, rhs.password).append(appRespId, rhs.appRespId).append(billToSiteUseId, rhs.billToSiteUseId).append(shipToSiteUseId, rhs.shipToSiteUseId).append(additionalProperties, rhs.additionalProperties).append(appUserId, rhs.appUserId).append(endCustSiteUseId, rhs.endCustSiteUseId).append(region, rhs.region).append(currencyCode, rhs.currencyCode).append(items, rhs.items).isEquals();
    }

}

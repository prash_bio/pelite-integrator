
package com.arrow.pricing.model.request;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "sourceLineReference",
    "pricingDate",
    "itemId",
    "arrowPartnumber",
    "customerPartNumber",
    "warehouseId",
    "quantity"
})
public class Item {

    @JsonProperty("sourceLineReference")
    private Integer sourceLineReference;
    @JsonProperty("pricingDate")
    private Object pricingDate;
    @JsonProperty("itemId")
    private Integer itemId;
    @JsonProperty("arrowPartnumber")
    private String arrowPartnumber;
    @JsonProperty("customerPartNumber")
    private String customerPartNumber;
    @JsonProperty("warehouseId")
    private Integer warehouseId;
    @JsonProperty("quantity")
    private Integer quantity;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("sourceLineReference")
    public Integer getSourceLineReference() {
        return sourceLineReference;
    }

    @JsonProperty("sourceLineReference")
    public void setSourceLineReference(Integer sourceLineReference) {
        this.sourceLineReference = sourceLineReference;
    }

    @JsonProperty("pricingDate")
    public Object getPricingDate() {
        return pricingDate;
    }

    @JsonProperty("pricingDate")
    public void setPricingDate(Object pricingDate) {
        this.pricingDate = pricingDate;
    }

    @JsonProperty("itemId")
    public Integer getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("arrowPartnumber")
    public String getArrowPartnumber() {
        return arrowPartnumber;
    }

    @JsonProperty("arrowPartnumber")
    public void setArrowPartnumber(String arrowPartnumber) {
        this.arrowPartnumber = arrowPartnumber;
    }

    @JsonProperty("customerPartNumber")
    public String getCustomerPartNumber() {
        return customerPartNumber;
    }

    @JsonProperty("customerPartNumber")
    public void setCustomerPartNumber(String customerPartNumber) {
        this.customerPartNumber = customerPartNumber;
    }

    @JsonProperty("warehouseId")
    public Integer getWarehouseId() {
        return warehouseId;
    }

    @JsonProperty("warehouseId")
    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    @JsonProperty("quantity")
    public Integer getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("sourceLineReference", sourceLineReference).append("pricingDate", pricingDate).append("itemId", itemId).append("arrowPartnumber", arrowPartnumber).append("customerPartNumber", customerPartNumber).append("warehouseId", warehouseId).append("quantity", quantity).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(itemId).append(pricingDate).append(customerPartNumber).append(quantity).append(warehouseId).append(sourceLineReference).append(additionalProperties).append(arrowPartnumber).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Item) == false) {
            return false;
        }
        Item rhs = ((Item) other);
        return new EqualsBuilder().append(itemId, rhs.itemId).append(pricingDate, rhs.pricingDate).append(customerPartNumber, rhs.customerPartNumber).append(quantity, rhs.quantity).append(warehouseId, rhs.warehouseId).append(sourceLineReference, rhs.sourceLineReference).append(additionalProperties, rhs.additionalProperties).append(arrowPartnumber, rhs.arrowPartnumber).isEquals();
    }

}

package com.arrow.pricing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PelitePriceResponse implements Serializable {

    public PelitePriceResponse() {
    }

    public PelitePriceResponse(String statusCode, String statusMessage, List<PelitePricingResponseItem> items) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
        this.items = items;
    }

    private String statusCode;

    private String statusMessage;

    private List<PelitePricingResponseItem> items;

    @JsonProperty("statusCode")
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @JsonProperty("statusMessage")
    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @JsonProperty("items")
    public List<PelitePricingResponseItem> getItems() {
        return items;
    }

    public void setItems(List<PelitePricingResponseItem> items) {
        this.items = items;
    }
}
package com.arrow.pricing.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PelitePricingResponseItem implements Serializable {

    public PelitePricingResponseItem() {

    }

    private String sourceLineReference;

    private String responseCode;

    private String responseMessage;

    private Long itemId;

    private Long warehouseId;

    private String pricingType;

    private String buyPriceBasis;

    private String resaleSourceCode;

    private String currencyCode;

    private Double resalePrice;

    private Double businessCost;

    private String overrideAllowed;

    private Long priceListId;

    private String priceListName;

    private Long bsaNumber;

    private Long bsaHeaderId;

    private Long bsaLineId;

    private String sqrNumber;

    private Long sqrLineId;

    private String sqrType;

    private Long remainingQuantity;

    private String customerSpecificPriceFlag;

    private Long endCustSiteUseId;

    private String customerPartNumber;

    private Long availabilityQuantity;

    private Long onSiteQuantity;

    private List<PelitePricingResponseBreaks> breaks;

    private List<PelitePricingResponseAddntlDetails> addlDetails;

    @JsonProperty("sourceLineReference")
    public String getSourceLineReference() {
        return sourceLineReference;
    }

    public void setSourceLineReference(String sourceLineReference) {
        this.sourceLineReference = sourceLineReference;
    }

    @JsonProperty("responseCode")
    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @JsonProperty("responseMessage")
    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    @JsonProperty("itemId")
    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("warehouseId")
    public Long getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Long warehouseId) {
        this.warehouseId = warehouseId;
    }

    @JsonProperty("pricingType")
    public String getPricingType() {
        return pricingType;
    }

    public void setPricingType(String pricingType) {
        this.pricingType = pricingType;
    }

    @JsonProperty("buyPriceBasis")
    public String getBuyPriceBasis() {
        return buyPriceBasis;
    }

    public void setBuyPriceBasis(String buyPriceBasis) {
        this.buyPriceBasis = buyPriceBasis;
    }

    @JsonProperty("resaleSourceCode")
    public String getResaleSourceCode() {
        return resaleSourceCode;
    }

    public void setResaleSourceCode(String resaleSourceCode) {
        this.resaleSourceCode = resaleSourceCode;
    }

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @JsonProperty("resalePrice")
    public Double getResalePrice() {
        return resalePrice;
    }

    public void setResalePrice(Double resalePrice) {
        this.resalePrice = resalePrice;
    }

    @JsonProperty("businessCost")
    public Double getBusinessCost() {
        return businessCost;
    }

    public void setBusinessCost(Double businessCost) {
        this.businessCost = businessCost;
    }

    @JsonProperty("overrideAllowed")
    public String getOverrideAllowed() {
        return overrideAllowed;
    }

    public void setOverrideAllowed(String overrideAllowed) {
        this.overrideAllowed = overrideAllowed;
    }

    @JsonProperty("priceListID")
    public Long getPriceListId() {
        return priceListId;
    }

    public void setPriceListId(Long priceListId) {
        this.priceListId = priceListId;
    }

    @JsonProperty("priceListName")
    public String getPriceListName() {
        return priceListName;
    }

    public void setPriceListName(String priceListName) {
        this.priceListName = priceListName;
    }

    @JsonProperty("BSANumber")
    public Long getBsaNumber() {
        return bsaNumber;
    }

    public void setBsaNumber(Long bsaNumber) {
        this.bsaNumber = bsaNumber;
    }

    @JsonProperty("BSAHeaderID")
    public Long getBsaHeaderId() {
        return bsaHeaderId;
    }

    public void setBsaHeaderId(Long bsaHeaderId) {
        this.bsaHeaderId = bsaHeaderId;
    }

    @JsonProperty("BSALineID")
    public Long getBsaLineId() {
        return bsaLineId;
    }

    public void setBsaLineId(Long bsaLineId) {
        this.bsaLineId = bsaLineId;
    }

    @JsonProperty("SQRNumber")
    public String getSqrNumber() {
        return sqrNumber;
    }

    public void setSqrNumber(String sqrNumber) {
        this.sqrNumber = sqrNumber;
    }

    @JsonProperty("SQRLineID")
    public Long getSqrLineId() {
        return sqrLineId;
    }

    public void setSqrLineId(Long sqrLineId) {
        this.sqrLineId = sqrLineId;
    }

    @JsonProperty("SQRType")
    public String getSqrType() {
        return sqrType;
    }

    public void setSqrType(String sqrType) {
        this.sqrType = sqrType;
    }

    @JsonProperty("remainingQuantity")
    public Long getRemainingQuantity() {
        return remainingQuantity;
    }

    public void setRemainingQuantity(Long remainingQuantity) {
        this.remainingQuantity = remainingQuantity;
    }

    @JsonProperty("customerSpecificPriceFlag")
    public String getCustomerSpecificPriceFlag() {
        return customerSpecificPriceFlag;
    }

    public void setCustomerSpecificPriceFlag(String customerSpecificPriceFlag) {
        this.customerSpecificPriceFlag = customerSpecificPriceFlag;
    }

    @JsonProperty("endCustSiteUseID")
    public Long getEndCustSiteUseId() {
        return endCustSiteUseId;
    }

    public void setEndCustSiteUseId(Long endCustSiteUseId) {
        this.endCustSiteUseId = endCustSiteUseId;
    }

    @JsonProperty("customerPartNumber")
    public String getCustomerPartNumber() {
        return customerPartNumber;
    }

    public void setCustomerPartNumber(String customerPartNumber) {
        this.customerPartNumber = customerPartNumber;
    }

    @JsonProperty("availabilityQuantity")
    public Long getAvailabilityQuantity() {
        return availabilityQuantity;
    }

    public void setAvailabilityQuantity(Long availabilityQuantity) {
        this.availabilityQuantity = availabilityQuantity;
    }

    @JsonProperty("onSiteQuantity")
    public Long getOnSiteQuantity() {
        return onSiteQuantity;
    }

    public void setOnSiteQuantity(Long onSiteQuantity) {
        this.onSiteQuantity = onSiteQuantity;
    }

    @JsonProperty("breaks")
    public List<PelitePricingResponseBreaks> getBreaks() {
        return breaks;
    }

    public void setBreaks(List<PelitePricingResponseBreaks> breaks) {
        this.breaks = breaks;
    }

    @JsonProperty("addlDetails")
    public List<PelitePricingResponseAddntlDetails> getAddlDetails() {
        return addlDetails;
    }

    public void setAddlDetails(List<PelitePricingResponseAddntlDetails> addlDetails) {
        this.addlDetails = addlDetails;
    }
}
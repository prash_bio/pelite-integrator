
package com.arrow.pricing.model.response;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "quantityFrom",
    "quantityTo",
    "resalePrice"
})
public class Break {

    @JsonProperty("quantityFrom")
    private Long quantityFrom;
    @JsonProperty("quantityTo")
    private Long quantityTo;
    @JsonProperty("resalePrice")
    private Double resalePrice;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("quantityFrom")
    public Long getQuantityFrom() {
        return quantityFrom;
    }

    @JsonProperty("quantityFrom")
    public void setQuantityFrom(Long quantityFrom) {
        this.quantityFrom = quantityFrom;
    }

    @JsonProperty("quantityTo")
    public Long getQuantityTo() {
        return quantityTo;
    }

    @JsonProperty("quantityTo")
    public void setQuantityTo(Long quantityTo) {
        this.quantityTo = quantityTo;
    }

    @JsonProperty("resalePrice")
    public Double getResalePrice() {
        return resalePrice;
    }

    @JsonProperty("resalePrice")
    public void setResalePrice(Double resalePrice) {
        this.resalePrice = resalePrice;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("quantityFrom", quantityFrom).append("quantityTo", quantityTo).append("resalePrice", resalePrice).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(quantityFrom).append(additionalProperties).append(quantityTo).append(resalePrice).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Break) == false) {
            return false;
        }
        Break rhs = ((Break) other);
        return new EqualsBuilder().append(quantityFrom, rhs.quantityFrom).append(additionalProperties, rhs.additionalProperties).append(quantityTo, rhs.quantityTo).append(resalePrice, rhs.resalePrice).isEquals();
    }

}

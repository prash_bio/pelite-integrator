
package com.arrow.pricing.model.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "resalePrice",
    "addlDetails",
    "sourceLineReference",
    "responseCode",
    "itemId",
    "warehouseId",
    "pricingType",
    "resaleSourceCode",
    "resaleTypeCode",
    "currencyCode",
    "overrideAllowed",
    "endCustSiteUseId",
    "customerPartNumber",
    "breaks"
})
public class Item {

    @JsonProperty("resalePrice")
    private Double resalePrice;
    @JsonProperty("addlDetails")
    private List<Object> addlDetails = new ArrayList<Object>();
    @JsonProperty("sourceLineReference")
    private String sourceLineReference;
    @JsonProperty("responseCode")
    private String responseCode;
    @JsonProperty("itemId")
    private Integer itemId;
    @JsonProperty("warehouseId")
    private Integer warehouseId;
    @JsonProperty("pricingType")
    private String pricingType;
    @JsonProperty("resaleSourceCode")
    private String resaleSourceCode;
    @JsonProperty("resaleTypeCode")
    private String resaleTypeCode;
    @JsonProperty("currencyCode")
    private String currencyCode;
    @JsonProperty("overrideAllowed")
    private String overrideAllowed;
    @JsonProperty("endCustSiteUseId")
    private Integer endCustSiteUseId;
    @JsonProperty("customerPartNumber")
    private String customerPartNumber;
    @JsonProperty("breaks")
    private List<Break> breaks = new ArrayList<Break>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("resalePrice")
    public Double getResalePrice() {
        return resalePrice;
    }

    @JsonProperty("resalePrice")
    public void setResalePrice(Double resalePrice) {
        this.resalePrice = resalePrice;
    }

    @JsonProperty("addlDetails")
    public List<Object> getAddlDetails() {
        return addlDetails;
    }

    @JsonProperty("addlDetails")
    public void setAddlDetails(List<Object> addlDetails) {
        this.addlDetails = addlDetails;
    }

    @JsonProperty("sourceLineReference")
    public String getSourceLineReference() {
        return sourceLineReference;
    }

    @JsonProperty("sourceLineReference")
    public void setSourceLineReference(String sourceLineReference) {
        this.sourceLineReference = sourceLineReference;
    }

    @JsonProperty("responseCode")
    public String getResponseCode() {
        return responseCode;
    }

    @JsonProperty("responseCode")
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @JsonProperty("itemId")
    public Integer getItemId() {
        return itemId;
    }

    @JsonProperty("itemId")
    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    @JsonProperty("warehouseId")
    public Integer getWarehouseId() {
        return warehouseId;
    }

    @JsonProperty("warehouseId")
    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    @JsonProperty("pricingType")
    public String getPricingType() {
        return pricingType;
    }

    @JsonProperty("pricingType")
    public void setPricingType(String pricingType) {
        this.pricingType = pricingType;
    }

    @JsonProperty("resaleSourceCode")
    public String getResaleSourceCode() {
        return resaleSourceCode;
    }

    @JsonProperty("resaleSourceCode")
    public void setResaleSourceCode(String resaleSourceCode) {
        this.resaleSourceCode = resaleSourceCode;
    }

    @JsonProperty("resaleTypeCode")
    public String getResaleTypeCode() {
        return resaleTypeCode;
    }

    @JsonProperty("resaleTypeCode")
    public void setResaleTypeCode(String resaleTypeCode) {
        this.resaleTypeCode = resaleTypeCode;
    }

    @JsonProperty("currencyCode")
    public String getCurrencyCode() {
        return currencyCode;
    }

    @JsonProperty("currencyCode")
    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @JsonProperty("overrideAllowed")
    public String getOverrideAllowed() {
        return overrideAllowed;
    }

    @JsonProperty("overrideAllowed")
    public void setOverrideAllowed(String overrideAllowed) {
        this.overrideAllowed = overrideAllowed;
    }

    @JsonProperty("endCustSiteUseId")
    public Integer getEndCustSiteUseId() {
        return endCustSiteUseId;
    }

    @JsonProperty("endCustSiteUseId")
    public void setEndCustSiteUseId(Integer endCustSiteUseId) {
        this.endCustSiteUseId = endCustSiteUseId;
    }

    @JsonProperty("customerPartNumber")
    public String getCustomerPartNumber() {
        return customerPartNumber;
    }

    @JsonProperty("customerPartNumber")
    public void setCustomerPartNumber(String customerPartNumber) {
        this.customerPartNumber = customerPartNumber;
    }

    @JsonProperty("breaks")
    public List<Break> getBreaks() {
        return breaks;
    }

    @JsonProperty("breaks")
    public void setBreaks(List<Break> breaks) {
        this.breaks = breaks;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("resalePrice", resalePrice).append("addlDetails", addlDetails).append("sourceLineReference", sourceLineReference).append("responseCode", responseCode).append("itemId", itemId).append("warehouseId", warehouseId).append("pricingType", pricingType).append("resaleSourceCode", resaleSourceCode).append("resaleTypeCode", resaleTypeCode).append("currencyCode", currencyCode).append("overrideAllowed", overrideAllowed).append("endCustSiteUseId", endCustSiteUseId).append("customerPartNumber", customerPartNumber).append("breaks", breaks).append("additionalProperties", additionalProperties).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(resaleSourceCode).append(customerPartNumber).append(breaks).append(overrideAllowed).append(resaleTypeCode).append(responseCode).append(itemId).append(addlDetails).append(warehouseId).append(resalePrice).append(sourceLineReference).append(additionalProperties).append(endCustSiteUseId).append(currencyCode).append(pricingType).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Item) == false) {
            return false;
        }
        Item rhs = ((Item) other);
        return new EqualsBuilder().append(resaleSourceCode, rhs.resaleSourceCode).append(customerPartNumber, rhs.customerPartNumber).append(breaks, rhs.breaks).append(overrideAllowed, rhs.overrideAllowed).append(resaleTypeCode, rhs.resaleTypeCode).append(responseCode, rhs.responseCode).append(itemId, rhs.itemId).append(addlDetails, rhs.addlDetails).append(warehouseId, rhs.warehouseId).append(resalePrice, rhs.resalePrice).append(sourceLineReference, rhs.sourceLineReference).append(additionalProperties, rhs.additionalProperties).append(endCustSiteUseId, rhs.endCustSiteUseId).append(currencyCode, rhs.currencyCode).append(pricingType, rhs.pricingType).isEquals();
    }

}

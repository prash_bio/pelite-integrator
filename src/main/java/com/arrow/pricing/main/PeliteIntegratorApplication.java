package com.arrow.pricing.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.arrow.pricing")
public class PeliteIntegratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(PeliteIntegratorApplication.class, args);
	}
}

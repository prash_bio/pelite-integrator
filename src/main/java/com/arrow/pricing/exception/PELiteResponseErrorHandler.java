package com.arrow.pricing.exception;

import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

/**
 * Created by prashant.patil on 18/01/2018.
 */
public class PELiteResponseErrorHandler extends DefaultResponseErrorHandler {

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        //conversion logic for decoding conversion
        InputStream inputStream = (InputStream) response.getBody();
        Scanner scanner = new Scanner(inputStream);
        scanner.useDelimiter("\\Z");
        String data = "The Exception is : ";
        if (scanner.hasNext())
            data = scanner.next();
        System.out.println(data);
        System.out.println(data);
    }
}
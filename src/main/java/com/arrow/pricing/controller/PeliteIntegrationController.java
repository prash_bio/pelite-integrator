package com.arrow.pricing.controller;

import com.arrow.pricing.model.PelitePriceRequest;
import com.arrow.pricing.model.PelitePriceResponse;
import com.arrow.pricing.model.request.PeLiteRequest;
import com.arrow.pricing.model.response.PeLiteResponse;
import com.arrow.pricing.service.PeliteIntegrationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@RestController
@RequestMapping(path = "/pe-lite")
@Api(value = "PE-Lite Integration Wrapper API", consumes = MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
public class PeliteIntegrationController {

    @Autowired(required = true)
    private final PeliteIntegrationService peliteIntegrationService;

    public PeliteIntegrationController(PeliteIntegrationService peliteIntegrationService) {
        this.peliteIntegrationService = peliteIntegrationService;
    }

    @RequestMapping(value = "/price", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation("Retrieves the price for specified item from the PE-Lite")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "OK", response = PelitePriceResponse.class)})
    public PeLiteResponse getPrice(@RequestBody PeLiteRequest peLiteRequest) throws Exception {
        return peliteIntegrationService.getPrice(peLiteRequest);
    }

}
package com.arrow.pricing.service;

import com.arrow.pricing.exception.PELiteResponseErrorHandler;
import com.arrow.pricing.model.PeliteAuthResponse;
import com.arrow.pricing.model.PelitePriceResponse;
import com.arrow.pricing.model.request.PeLiteRequest;
import com.arrow.pricing.model.response.PeLiteResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class PeliteIntegrationService {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }

    public PeLiteResponse getPrice(PeLiteRequest peLiteRequest) throws Exception {
        RestTemplate restTemplate = restTemplate();
        PeliteAuthResponse peliteAuthResponse = invoke(restTemplate);

        System.out.println("PE-Lite Price Request JSON = " + printRequestJSON(peLiteRequest));

        //TODO Refactor me. I need a cosy home. Please take me to YAML file.
        String pelitePriceServiceUrl = "https://qarwsvc.arrow.com/qa/peservice/myarrow";
        //String pelitePriceServiceUrl = "https://qarwsvc.arrow.com/qa/peservice/myarrow";

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        String authorizationToken = buildAuthorizationToken(peliteAuthResponse);
        System.out.println("authorizationToken = " + authorizationToken);
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
        headers.set("Authorization", String.format("Bearer %s", peliteAuthResponse.getAccessToken()));


        MappingJackson2HttpMessageConverter jsonHttpMessageConverter = new MappingJackson2HttpMessageConverter();
        jsonHttpMessageConverter.getObjectMapper().configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        restTemplate.getMessageConverters().add(jsonHttpMessageConverter);
        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
        restTemplate.setErrorHandler(new PELiteResponseErrorHandler());


        HttpEntity<PeLiteRequest> request = new HttpEntity<PeLiteRequest>(peLiteRequest, headers);

        PeLiteResponse peLiteResponse = null;

        peLiteResponse = restTemplate.postForObject(pelitePriceServiceUrl, request, PeLiteResponse.class);

        System.out.println("PE-Lite Price Response JSON = " + printResponseJSON(peLiteResponse));

        return peLiteResponse;
    }

    public PeliteAuthResponse invoke(RestTemplate restTemplate) throws Exception {

        //TODO Refactor me. I need a cosy home. Please take me to YAML file.
        MultiValueMap<String, String> authTokenRequest = new LinkedMultiValueMap<String, String>();
        authTokenRequest.set("grant_type", "client_credentials");
        authTokenRequest.set("client_id", "eb09ac4c-2b47-44de-9ab7-b65d1c414368");
        authTokenRequest.set("client_secret", "ff1a0dc9-43e0-435a-8191-9bbccd63d515");

        String peliteAuthUrl = "https://qarwsvc.arrow.com/api/oauth/token";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(authTokenRequest, headers);

        List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
        messageConverters.add(new MappingJackson2HttpMessageConverter());
        messageConverters.add(new FormHttpMessageConverter());
        restTemplate.setMessageConverters(messageConverters);

        PeliteAuthResponse peliteAuthResponse = restTemplate.postForObject(peliteAuthUrl, request, PeliteAuthResponse.class);

        return peliteAuthResponse;
    }

    private String buildAuthorizationToken(PeliteAuthResponse peliteAuthResponse) {
        StringBuilder token = new StringBuilder("Bearer");
        token.append(" ");
        token.append(peliteAuthResponse.getAccessToken());

        return token.toString();
    }

    private String printRequestJSON(PeLiteRequest peLiteRequest) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(peLiteRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonString;
    }

    private String printResponseJSON(PeLiteResponse peLiteResponse) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        String jsonString = null;
        try {
            jsonString = mapper.writeValueAsString(peLiteResponse);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonString;
    }
}